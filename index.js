// Array Traversal

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// Put it in an array
/*
	Syntax: let/ const arrayName = [elementA, elementB, ...]
*/

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

// Common examples of an array
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["Ace", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Alienware"];
console.log(grades);
console.log(computerBrands);

// Alternative ways of writing an array

let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake react'
];

console.log(myTasks)

// Create an array with values from variables

let city1 = "Tokyo"
let city2 = "Jakarta"
let city3 = "Manila"

let cities = [city1, city2, city3];
console.log(cities);

//  ,lenght property

console.log(myTasks.length); //result: 4

console.log(cities.length); //result: 3

//  length property can be used in a string
let fullName = "Lisa Manoban"
console.log(fullName.length); //result: 12

myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks); //result: remove bake react

// another example using decrementation
cities.length--;
console.log(cities); //result: removed Manila

// NOT APPLCIBLE TO STRING ONLY IN ARRAY

// we can't do the same with strings
fullName.length = fullName.length-1;
console.log(fullName.length); //result none still 12 character

fullName.length--;
console.log(fullName.length); //result still 12 character

// forcibly adding in the lenght of our array
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles); //result: empty or undefined

// Reading from Arrays
/*
	Syntax:
	arrayName[index];
*/

// index starts with 0
console.log(grades[0]); //result: 98.5
console.log(computerBrands[3]); //result: Neo
console.log(grades[20]); //result: Undefined

let lakersLegend = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]

console.log(lakersLegend)
console.log(lakersLegend[1]); //result: Shaq
console.log(lakersLegend[3]); //result: Magic

let currentLakers = lakersLegend[2];
console.log(currentLakers); // result: Lebron

// reassigning
console.log("Array before reassignment: ")
console.log(lakersLegend);
console.log("Array after reassingment")
lakersLegend[3] = "Gasol";
console.log(lakersLegend)

// accessing the last element of an array
let bullsLegend = ["Jorda", "Pippen", "Rodman", "Rose", "Kukoc"];
console.log(bullsLegend.length);
// number of elements = 5 in bullsLegend
	// this starts counting at 1

// total number of index = 4
	// index is start at 0

console.log(bullsLegend);
let lastElementsIndex = bullsLegend.length-4;

console.log(bullsLegend[lastElementsIndex]); //result: Kukoc

// Adding items into the array

let newArr = [];
console.log(newArr[0]); // result: undefined

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[2] = "Zack Fair";
console.log(newArr);

//result: is reassigning 
newArr[newArr.length] = "Barret Wallace";
console.log(newArr);
//newArr[3] = "Barret Wallace"

// using this length-1 replace for barret wallace
newArr[newArr.length-1] = "Aerith Gainsborough"

// reassinging elements in array using const
const family = ["Edward", "Alphonse", "dog"]
console.log("Current Family " + family)

family[2] = "Nina";
console.log("New Family: " + family)

// Looping over an array

for(let index = 0; index < newArr.length; index++){

	console.log(newArr[index]);

};

 let numArr = [5, 12, 30, 46, 60]

for(let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	} else {
		console.log(numArr[index] + " is not divisible by 5")
	}
};

// Multidimensional Array
let chessBoard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]
];

console.log(chessBoard);
console.log("Pawn moves to: " + chessBoard[1][5])

